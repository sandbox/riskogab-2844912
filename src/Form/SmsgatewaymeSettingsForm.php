<?php

namespace Drupal\smsgatewayme\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure smsgatewayme settings for this site.
 */
class SmsgatewaymeSettingsForm extends ConfigFormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smsgatewayme_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'smsgatewayme.settings',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('smsgatewayme.settings');

    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('SMSGateway.me username'),
      '#default_value' => $config->get('username'),
    );  

    $form['password'] = array(
      '#type' => 'password',
      '#title' => $this->t('SMSGateway.me password'),
    );

    /*$form['example_thing'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Things'),
      '#default_value' => $config->get('things'),
    );  

    $form['other_things'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Other things'),
      '#default_value' => $config->get('other_things'),
    );*/  

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration
    $this->config('smsgatewayme.settings')
      // Set the submitted configuration setting
      ->set('things', $form_state->getValue('username'))
      // You can set multiple configurations at once by making
      // multiple calls to set()
      ->set('other_things', $form_state->getValue('password'))
      ->save();

    // Example:
    /*$this->config('smsgatewayme.settings')
      // Set the submitted configuration setting
      ->set('things', $form_state->getValue('example_thing'))
      // You can set multiple configurations at once by making
      // multiple calls to set()
      ->set('other_things', $form_state->getValue('other_things'))
      ->save();*/

    parent::submitForm($form, $form_state);
  }
}
