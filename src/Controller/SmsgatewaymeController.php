<?php
namespace Drupal\smsgatewayme\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

class SmsgatewaymeController extends ControllerBase {
  public function __construct() {
    
  }
  
  public function admin_settings() {
    return array(
        '#type' => 'markup',
        '#markup' => $this->t('Hello, World!'),
    );
  }
}
